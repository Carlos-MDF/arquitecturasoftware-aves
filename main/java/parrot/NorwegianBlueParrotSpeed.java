package parrot;

public class NorwegianBlueParrotSpeed implements SpeedCalculator {

	public double calculateSpeed(Parrot parrot) {
		return (parrot.isNailed) ? 0 : parrot.getBaseSpeed(parrot.voltage);
	}

}
