package parrot;

public class EuropeanParrotSpeed implements SpeedCalculator {

	public double calculateSpeed(Parrot parrot) {
		return parrot.getBaseSpeed();
	}

}
