package parrot;

public interface SpeedCalculator {
	public double calculateSpeed(Parrot parrot);
}
