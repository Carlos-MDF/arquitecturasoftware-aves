package parrot;

public class AfricanParrotSpeed implements SpeedCalculator {

	public double calculateSpeed(Parrot parrot) {
		return Math.max(0, parrot.getBaseSpeed() - parrot.getLoadFactor() * parrot.numberOfCoconuts);
	}

}
