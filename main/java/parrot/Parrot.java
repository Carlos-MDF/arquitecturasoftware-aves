package parrot;

public class Parrot {

    private ParrotTypeEnum type;
    int numberOfCoconuts = 0;
    double voltage;
    boolean isNailed;


    public Parrot(ParrotTypeEnum _type, int numberOfCoconuts, double voltage, boolean isNailed) {
        this.type = _type;
        this.numberOfCoconuts = numberOfCoconuts;
        this.voltage = voltage;
        this.isNailed = isNailed;
    }

    public double getParrotSpeed() {
    	double parrotSpeed = 0;
    	SpeedCalculator speedCalculator = createSpeedCalculator();
    	parrotSpeed = speedCalculator.calculateSpeed(this);
    	return parrotSpeed;
    }

	private SpeedCalculator createSpeedCalculator() {
		SpeedCalculator speedCalculator = null;
        switch(type) {
            case EUROPEAN:
            	return speedCalculator = new EuropeanParrotSpeed();
            case AFRICAN:
            	return speedCalculator = new AfricanParrotSpeed();
            case NORWEGIAN_BLUE:
            	return speedCalculator= new NorwegianBlueParrotSpeed();
        }
        throw new RuntimeException("Should be unreachable");
	}

	double getBaseSpeed(double voltage) {
        return Math.min(24.0, voltage*getBaseSpeed());
    }

    double getLoadFactor() {
        return 9.0;
    }

    double getBaseSpeed() {
        return 12.0;
    }


}
